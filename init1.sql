-- Create the database if it doesn't exist
-- CREATE DATABASE IF NOT EXISTS userlogin;
-- USE userlogin;

CREATE DATABASE IF NOT EXISTS todoapp;
USE todoapp;
-- users for login purposes
CREATE TABLE users (
    id INT AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(50) NOT NULL,
    password VARCHAR(255) NOT NULL
);

INSERT INTO users (username, password) VALUES ('todoapp', 'todoapp');
-- change user and password in upper line 


-- task for todo app
CREATE TABLE tasks (
    id INT AUTO_INCREMENT PRIMARY KEY,
    employee_name VARCHAR(255) NOT NULL,
    task_description TEXT,
    task_date DATE
);