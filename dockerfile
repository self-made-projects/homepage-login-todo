# TO BUILD THIS noscopev6/ubuntu-php-todo IMAGE
# Setup php enviroment inside docker file
# Use the official Ubuntu as a parent image
FROM ubuntu:latest

# Update the package list and install necessary software
RUN apt-get update && apt-get install -y software-properties-common
RUN add-apt-repository ppa:ondrej/php

# Update the package list again
RUN apt-get update

# Install Nginx, PHP 8.0, and necessary extensions
RUN apt-get install -y nginx php8.0-fpm php8.0-mysql

RUN rm -rf /var/www/html/*
RUN rm -rf  /etc/nginx/sites-available/default
COPY ./nginx-default.conf /etc/nginx/sites-available/default
#RUN cd /var/www/html && wget https://github.com/vrana/adminer/releases/download/v4.8.1/adminer-4.8.1.php -O abc.php
RUN chown -R www-data:www-data /var/www/html
# Expose port 80 for Nginx
EXPOSE 80

# Start Nginx and PHP-FPM
CMD service nginx start && service php8.0-fpm start && tail -f /dev/null

# Start Nginx when the container starts
#CMD ["nginx", "-g", "daemon off;"]