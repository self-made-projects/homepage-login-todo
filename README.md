## Application
This is a 2 tier TODO Application 
    Enter Your Name > Task description > Select Date of Task
Task Details Store in My Sql DB 

 
## DB 
## this should be execute in db to add tables and column (to store task data in database)
CREATE TABLE tasks (
    id INT AUTO_INCREMENT PRIMARY KEY,
    employee_name VARCHAR(255) NOT NULL,
    task_description TEXT,
    task_date DATE
);

## Dockerfile 
Using Base Image as ubuntu and install required packages
Build this image with name [todoapp-php] mentioned in compose file 
Do not forget to Add that build file in Docker-compose file 

## Docker-compose file 
As this is a 2 tier TODO Application 
Frontend - using my own image from dockerhub [or you can build it yourself mentioned in dockerfile]
         - 8444 port is changable
         - 
Backend - using official mysql container from dockerhub and using init.sql to create a db and table in mysql db
        - do not change the db username and password becasue you have to change it from the application php files .
        

## DB info stored in application file (find below line in all php file of application )
    $host = "db";       
    $username = "todoapp"; 
    $password = "todoapp";
    $database = "todoapp";


## HOW TO RUN IT SIMPLY
- install docker &  docker compose 
        apt install docker.io -y && apt install docker-compose -y 
- clone the project and go inside the dir 
- docker-compose up -d 
- URL - http://localhost:8444 (localhost is you server IP )

